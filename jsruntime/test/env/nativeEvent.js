OpenBlock.registerNativeEvents([
    { "name": "touchstart", "argType": "Vector2" },
    { "name": "touchmove", "argType": "Vector2" },
    { "name": "touchcancel", "argType": "Vector2" },
    { "name": "touchend", "argType": "Vector2" },
    { "name": "click", "argType": "Vector2" },
    { "name": "longpress", "argType": "Vector2" },
    { "name": "swipe" },
]);
