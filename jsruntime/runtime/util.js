/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
// ++++ for OpenHarmony +++
// import util from '@ohos.util'
// let _TextDecoder = util.TextDecoder;
// ---- for OpenHarmony ---

// ++++ for nodejs +++
// import util from 'util'
// let _TextDecoder = util.TextDecoder;
// ---- for nodejs ---

let _TextDecoder = TextDecoder;
export { _TextDecoder as TextDecoder };