/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */

 class Analyser {
    visitCode(code, ctx, fCtx) {
    }

    visitModuleStart(m, ctx) {
        this.visitStart(m, ctx);
    }
    visitModuleEnd(m, ctx) {
        this.visitEnd(m, ctx);
    }

    visitFSMStart(fsm, ctx) {
        this.visitStart(fsm, ctx);
    }
    visitFSMEnd(fsm, ctx) {
        this.visitEnd(fsm, ctx);
    }

    visitStateStart(s, ctx) {
        this.visitStart(s, ctx);
    }
    visitStateEnd(s, ctx) {
        this.visitEnd(s, ctx);
    }

    visitFunctionStart(f, ctx) {
        this.visitStart(f, ctx);
    }
    visitFunctionEnd(f, ctx) {
        this.visitEnd(f, ctx);
    }

    visitEventHandlerStart(code, ctx) {
        this.visitStart(code, ctx);
    }
    visitEventHandlerEnd(code, ctx) {
        this.visitEnd(code, ctx);
    }

    visitMessageHandlerStart(code, ctx) {
        this.visitStart(code, ctx);
    }
    visitMessageHandlerEnd(code, ctx) {
        this.visitEnd(code, ctx);
    }

    visitStatementStart(code, ctx, fCtx) {
        this.visitStart(code, ctx, fCtx);
    }
    visitStatementEnd(code, ctx, fCtx) {
        this.visitEnd(code, ctx, fCtx);
    }

    visitRepeatTimesStart(l, ctx, fCtx) {
        this.visitStart(l, ctx, fCtx);
    }
    visitRepeatTimesEnd(l, ctx, fCtx) {
        this.visitEnd(l, ctx, fCtx);
    }

    visitIfFlowStart(l, ctx, fCtx) {
        this.visitStart(l, ctx, fCtx);
    }
    visitIfFlowEnd(l, ctx, fCtx) {
        this.visitEnd(l, ctx, fCtx);
    }

    visitWhileUntilLoopStart(l, ctx, fCtx) {
        this.visitStart(l, ctx, fCtx);
    }
    visitWhileUntilLoopEnd(l, ctx, fCtx) {
        this.visitEnd(l, ctx, fCtx);
    }
    visitForLoopStart(l, ctx, fCtx) {
        this.visitStart(l, ctx, fCtx);
    }
    visitForLoopEnd(l, ctx, fCtx) {
        this.visitEnd(l, ctx, fCtx);
    }
    visitStart(arg, ctx, fCtx) { }
    visitEnd(arg, ctx, fCtx) { }
    finish() { }
}
class RootAnalyser extends Analyser {
    analyserList;
    constructor(analyserList) {
        super();
        this.analyserList = analyserList;
    }

    visitCode(code, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitCode(code, ctx, fCtx);
        }
    }

    visitModuleStart(m, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitModuleStart(m, ctx);
        }
    }
    visitModuleEnd(m, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitModuleEnd(m, ctx);
        }
    }

    visitFSMStart(fsm, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitFSMStart(fsm, ctx);
        }
    }
    visitFSMEnd(fsm, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitFSMEnd(fsm, ctx);
        }
    }

    visitStateStart(s, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitStateStart(s, ctx);
        }
    }
    visitStateEnd(s, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitStateEnd(s, ctx);
        }
    }

    visitFunctionStart(f, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitFunctionStart(f, ctx);
        }
    }
    visitFunctionEnd(f, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitFunctionEnd(f, ctx);
        }
    }

    visitEventHandlerStart(code, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitEventHandlerStart(code, ctx);
        }
    }
    visitEventHandlerEnd(code, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitEventHandlerEnd(code, ctx);
        }
    }

    visitMessageHandlerStart(code, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitMessageHandlerStart(code, ctx);
        }
    }
    visitMessageHandlerEnd(code, ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitMessageHandlerEnd(code, ctx);
        }
    }

    visitStatementStart(code, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitStatementStart(code, ctx, fCtx);
        }
    }
    visitStatementEnd(code, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitStatementEnd(code, ctx, fCtx);
        }
    }

    visitRepeatTimesStart(l, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitRepeatTimesStart(l, ctx, fCtx);
        }
    }
    visitRepeatTimesEnd(l, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitRepeatTimesEnd(l, ctx, fCtx);
        }
    }

    visitIfFlowStart(l, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitIfFlowStart(l, ctx, fCtx);
        }
    }
    visitIfFlowEnd(l, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitIfFlowEnd(l, ctx, fCtx);
        }
    }

    visitForLoopStart(l, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitForLoopStart(l, ctx, fCtx);
        }
    }
    visitForLoopEnd(l, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitForLoopEnd(l, ctx, fCtx);
        }
    }
    visitWhileUntilLoopStart(l, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitWhileUntilLoopStart(l, ctx, fCtx);
        }
    }
    visitWhileUntilLoopEnd(l, ctx, fCtx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.visitWhileUntilLoopEnd(l, ctx, fCtx);
        }
    }
    finish(ctx) {
        for (let i = 0; i < this.analyserList.length; i++) {
            let analyser = this.analyserList[i];
            analyser.finish(ctx);
        }
    }
}