/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */

// window.addEventListener("beforeunload", function (e) {
//     e.preventDefault();
//     var dialogText = '离开此网站？系统可能不会保存您所做的更改。';
//     e.returnValue = dialogText;
//     return dialogText;
// });
window.FileInterface = Object.assign({
    saveExeFile(e, arrbuf) {
        if (e) {
            throw e;
        }
        FileOD.Save(/*src.name + '.xe'*/'ub.xe', new Blob([arrbuf], {
            type: 'application/octet-stream'
        }));
    },
    saveLibFile(srcname, e, blob) {
        if (e) {
            throw e;
        }
        FileOD.Save(srcname + '.xl', blob);
    },
    saveSrcFile(srcname, e, src_str) {
        if (e) {
            throw e;
        }
        FileOD.Save(srcname + '.xs', src_str);
    },
    loadSrcFiles(cb) {
        let _loadFiles = (files, cb) => {
            let parsedFile = [];
            let errors = [];

            function checkFinish() {
                if (parsedFile.length + errors.length === files.length) {
                    if (errors.length > 0) {
                        throw new Error(errors);
                    }
                    try {
                        OpenBlock.BlocklyParser.addFiles(parsedFile);
                    } catch (e) {
                        console.error(e);
                    }
                    if (typeof (cb) === 'function') {
                        cb(parsedFile)
                    }
                }
            }
            files.forEach((f) => {
                // let fname = f.name.toLowerCase();
                let reader = new FileReader();
                reader.readAsText(f);
                reader.onloadend = (evt) => {
                    try {
                        let json = JSON.parse(evt.target.result);
                        if (json.type === 'src' || json.type === 'lib') {
                            parsedFile.push(json);
                        } else {
                            throw new Error(`未知文件类型:${json.name}-${json.type}`);
                        }
                    } catch (e) {
                        errors.push(e);
                    }
                    checkFinish();
                };
            });
        };
        FileOD.Open('.xs,.xl', 'File', (files) => {
            files = Object.values(files);
            _loadFiles(files, () => {
                if (cb) {
                    cb(files);
                }
            });
        }, true);
    },
    loadExcel(cb) {
        FileOD.Open('.xlsx', 'Array​Buffer', (arrayBufferArray) => {
            cb(arrayBufferArray);
        }, true);
    }
}, window.FileInterface);
function requestText(url, callback, async) {
    $.ajax({
        type: 'GET',
        url,
        dataType: 'text',
        async: typeof (async) == 'undefined' ? true : async,
        success: function (data) {
            callback(data);
        },
        error(xhr, status, err) {
            callback(null, xhr, status, err);
        }
    });
}
function requestBin(url, callback, async) {
    var oReq = new XMLHttpRequest();
    oReq.open("GET", url, typeof (async) == 'undefined' ? true : async);
    oReq.responseType = "arraybuffer";

    oReq.onload = function (oEvent) {
        var arrayBuffer = oReq.response;
        callback(arrayBuffer);
        //   // if you want to access the bytes:
        //   var byteArray = new Uint8Array(arrayBuffer);
        //   // ...

        //   // If you want to use the image in your DOM:
        //   var blob = new Blob([arrayBuffer], {type: "image/png"});
        //   var url = URL.createObjectURL(blob);
        //   someImageElement.src = url;

        //   // whatever...
    };

    oReq.send();
}
function showCreateVarDiv(callback) {
    return function (src, target) {
        window.UB_IDE.showVariableInfo = true;
        window.UB_IDE.createVarInfo = { src, name: "", target, type: '', wrap: "", baseData: [], callback, export: true };
        window.UB_IDE.avalibleVarTypes("");
    }
}
function asyncConfirm(msg, callback) {
    window.UB_IDE.$Modal.confirm({
        title: '确认',
        content: `<p>${msg}</p>`,
        onOk() {
            callback(true);
        },
        onCancel() {
            callback(false);
        }
    });
}
/**
 * @type {{name:string,icon:string}[]}
 */
let siderComponents = [];
$(function () {
    (function () {
        let stateToolbox, functionToolbox;
        let config;
        $.ajax({
            type: 'GET',
            url: 'core/xml/stateToolbox2.xml',
            dataType: 'text',
            async: false,
            success: function (data) {
                stateToolbox = data;
            }
        });
        $.ajax({
            type: 'GET',
            url: 'core/xml/functionToolbox.xml',
            dataType: 'text',
            async: false,
            success: function (data) {
                functionToolbox = data;
            }
        });
        $.ajax({
            url: 'config/openblock.json',
            dataType: 'text',
            async: false,
            success(data, status, xhr) {
                config = JSON.parse(data);
            }
        });
        function buildBlockly() {
            config.uiCallbacks = {
                addFsmVariable: showCreateVarDiv(OpenBlock.addFSMVariable),
                addStateVariable: showCreateVarDiv(OpenBlock.addStateVariable),
                confirm: asyncConfirm
            };
            config.toolbox = {
                state: stateToolbox,
                function: functionToolbox
            };
            OpenBlock.init({
                stubToolbox: false,
                uiCallbacks: {
                    addFsmVariable: showCreateVarDiv(OpenBlock.addFSMVariable),
                    addStateVariable: showCreateVarDiv(OpenBlock.addStateVariable),
                    confirm: asyncConfirm
                },
                extI18NPath: 'i18n/',
                toolbox: {
                    state: stateToolbox,
                    function: functionToolbox
                },
                blocklyOpt: config.blocklyOpt || {
                    grid: {
                        spacing: 25,
                        length: 3,
                        "colour": "#F3F3F3",
                        snap: false
                    },
                    move: {
                        scrollbars: true,
                        drag: true,
                        wheel: true
                    },
                    // maxBlocks: 65535,
                    zoom: {
                        'controls': true,
                        'wheel': true,
                        minScale: 0.7,
                    },
                    scrollbars: true,
                    sounds: false,
                    comments: true,
                    disable: true
                }
            });
        }
        buildBlockly();
    })();
});
function startUI() {
    window.UB_IDE = new Vue({
        el: '#openblock',
        data: {
            subwindows: [],
            showEditor: true,
            showProjectWindow: true,
            OpenBlock: OpenBlock,
            searchBlockId: '',
            createVarInfo: null,
            tabs: [],
            showingTabName: "",
            siderUsing: null,
            siderComponents: siderComponents,
            siderList: [],
            editingSrc: { src: {} },
            showSrcEditWindow: false,
            showRenameDialog: false,
            renameDialog: false,
            showVariableInfo: false,
            showErrorWindow: false,
            compiling: false,
            icp: null
        },
        mounted: function () {
            axios.get('/icp').then(({ data }) => {
                this.icp = data;
            });
            this.$nextTick(function () {
                hideOverallLoading();
            })
        },
        methods: {
            runProject() {
                this.tabs.forEach(tab => {
                    let t = tab.target;
                    if (t && t.saveCode) {
                        t.saveCode();
                    }
                });
                if (OpenBlock.currentConnector && OpenBlock.currentConnector.runProject) {
                    OpenBlock.currentConnector.runProject();
                }
            },
            addSubwindowComponent(componentName) {
                this.subwindows.push(componentName);
            },
            removeSubwindowComponent(componentName) {
                const index = this.subwindows.indexOf(componentName);
                if (index > -1) {
                    this.subwindows.splice(index, 1);
                }
            },
            ensureSubwindowComponent(componentName) {
                if (this.subwindows.indexOf(componentName) === -1) {
                    this.addSubwindowComponent(componentName);
                }
            },
            useConnector(connector) {
                OpenBlock.useConnector(connector);
            },
            triggleSider(componentName) {
                if (this.siderUsing === componentName) {
                    this.siderUsing = null;
                } else {
                    this.siderUsing = componentName;
                }
            },
            avalibleVarTypes(value) {
                function check(v1, v2) {
                    if (!v1) {
                        return false;
                    }
                    v1 = v1.toLowerCase();
                    return v1.indexOf(v2) > -1;
                }
                value = value.toLowerCase();
                let structTypes = OpenBlock.getAvailableTypes(this.createVarInfo.src);
                let newTypes = structTypes.filter(t => check(t[0], value) || check(t[1], value)).map(t => t[0]);
                let nativeTypes = Object.keys(OpenBlock.nativeTypes).filter(t => check(t, value) || check(Blockly.Msg[t], value));
                nativeTypes = nativeTypes.map(t => OpenBlock.i(t));
                this.createVarInfo.baseData = newTypes.concat(nativeTypes);
            },
            applyCreateVar() {
                let i = this.createVarInfo;
                let typeName = i.type;
                let allType = OpenBlock.getAvailableTypes(this.createVarInfo.src);
                let type = allType.find(t => t[0] === typeName);
                if (type) {
                    i.type = type[1];
                } else {
                    let t = Object.keys(OpenBlock.nativeTypes).find(t => OpenBlock.i(t) === typeName);
                    if (t) {
                        i.type = t;
                    } else {
                        throw Error(OpenBlock.i('未设置类型'));
                    }
                }
                i.callback(i.target, i);
                Vue.nextTick(function () {
                    this.showVariableInfo = false;
                });
            },
            loadFiles: function () {
                FileInterface.loadSrcFiles(() => {
                    OpenBlock.exportExePackage();
                });
            },
            newFile: function () {
                let f = OpenBlock.newSrc({
                    "env": []
                });
                OpenBlock.BlocklyParser.addFiles([f]);
            },
            editSrc(src) {
                this.showSrcEditWindow = false;
                Vue.nextTick(() => {
                    let newDeps = src.depends.concat([]);
                    let editingSrc = {
                        src,
                        warning: null,
                        name: src.name,
                        depends: newDeps,
                    };
                    this.editingSrc = editingSrc;
                    this.showSrcEditWindow = true;
                    this.$forceUpdate();
                });
            },
            submitEditingSrc() {
                let editingSrc = this.editingSrc;
                let src = editingSrc.src;
                let check =
                    OpenBlock.BlocklyParser.loadedFiles.srcs.find(s => (s != src && s.name === editingSrc.name));
                if (check) {
                    editingSrc.warning = "模块已存在";
                    return;
                } else {
                    editingSrc.src.name = editingSrc.name;
                    this.showSrcEditWindow = false;
                    this.editingSrc = { src: {} };
                }
                src.depends = editingSrc.depends;
                OpenBlock.BlocklyParser.updateDepends();
                this.$forceUpdate();
            },
            cleanEditingSrc() {
                this.showSrcEditWindow = false;
                this.editingSrc = { src: {} };
            },
            analyze: function () {
                OpenBlock.exportExePackage();
            },
            saveSrcFile: function (src) {
                this.tabs.forEach(tab => {
                    let t = tab.target;
                    if (t && t.saveCode) {
                        t.saveCode();
                    }
                });
                OpenBlock.serializeSrc(src, (e, src_str) => { FileInterface.saveSrcFile(src.name, e, src_str); });
            },
            // saveLibFile: function (src) {
            //     OpenBlock.exportExePackage();
            //     OpenBlock.serializeLib(src, (e, blob) => { FileInterface.saveLibFile(src.name, e, blob); });
            // },
            saveExeFile: function () {
                this.tabs.forEach(tab => {
                    let t = tab.target;
                    if (t && t.saveCode) {
                        t.saveCode();
                    }
                });
                OpenBlock.exportExePackage(FileInterface.saveExeFile);
            },
            addFSM: function (src) {
                OpenBlock.addFSM(src);
            },
            prompt(msg, oldValue, onok) {
                let self = this;
                this.renameDialog = {
                    title: msg,
                    oldValue: oldValue,
                    value: oldValue,
                    ok() {
                        onok(self.renameDialog.value);
                        self.showRenameDialog = false;
                    }, cancel() {
                        self.showRenameDialog = false;
                    }
                }
                this.showRenameDialog = true;
            },
            renameFSM: function (src, fsm) {
                this.prompt("重命名状态机类型", fsm.name, (v) => {
                    if (v) {
                        if (v === fsm.name) {
                            return;
                        }
                        let check = src.fsms.find(_fsm => _fsm.name === v);
                        if (check) {
                            Vue.nextTick(() => {
                                this.$Modal.error({
                                    title: "错误",
                                    content: "该名称已存在"
                                });
                            });
                        } else {
                            fsm.name = v;
                            OpenBlock.exportExePackage();
                        }
                    }
                });
            },
            renameState: function (fsm, state) {
                this.prompt("重命名状态", state.name, (v) => {
                    if (v) {
                        if (v === state.name) {
                            return;
                        }
                        let check = fsm.states.find(o => o.name === v);
                        if (check) {
                            Vue.nextTick(() => {
                                this.$Modal.error({
                                    title: "错误",
                                    content: "该名称已存在"
                                });
                            });
                        } else {
                            state.name = v;
                            OpenBlock.exportExePackage();
                        }
                    }
                });
            },
            renameStruct: function (src, st) {
                this.prompt("重命名数据结构", st.name, (v) => {
                    if (v) {
                        if (v === st.name) {
                            return;
                        }
                        let check = src.structs.find(o => o.name === v);
                        if (check) {
                            Vue.nextTick(() => {
                                this.$Modal.error({
                                    title: "错误",
                                    content: "该名称已存在"
                                });
                            });
                        } else {
                            st.name = v;
                            OpenBlock.exportExePackage();
                        }
                    }
                });
            },
            renameFunction: function (src, st) {
                this.prompt("重命名函数", st.name, (v) => {
                    if (v) {
                        if (v === st.name) {
                            return;
                        }
                        let check = src.functions.find(o => o.name === v);
                        if (check) {
                            Vue.nextTick(() => {
                                this.$Modal.error({
                                    title: "错误",
                                    content: "该名称已存在"
                                });
                            });
                        } else {
                            st.name = v;
                            OpenBlock.exportExePackage();
                        }
                    }
                });
            },
            renameFSMVariable: function (fsm, v) {
                let newName = prompt("name", v.name)
                if (newName) {
                    v.name = newName;
                    OpenBlock.exportExePackage();
                }
            },
            setStartState: function (fsm, state) {
                let index = fsm.states.indexOf(state);
                if (index != 0) {
                    let startState = fsm.states[0];
                    // fsm.states[index] = startState;
                    Vue.set(fsm.states, index, startState);
                    // fsm.states[0] = state;
                    Vue.set(fsm.states, 0, state);
                }
            },
            removeFSMVariable: function (fsm, v) {

                this.$Modal.confirm({
                    title: '删除',
                    content: `<p>删除变量 ${v.name}</p>`,
                    onOk() {
                        OpenBlock.removeFSMVariable(fsm, v);
                        OpenBlock.exportExePackage();
                    }
                });
            },
            removeFSM: function (src, fsm) {
                this.$Modal.confirm({
                    title: '删除',
                    content: `<p>删除状态机类型 ${fsm.name}</p>`,
                    onOk() {
                        for (let i = 0; i < src.fsms.length; i++) {
                            if (src.fsms[i] === fsm) {
                                src.fsms.splice(i, 1);
                            }
                        }
                        OpenBlock.exportExePackage();
                    }
                });
            },
            addFsmVariable: function (src, fsm) {
                showCreateVarDiv(OpenBlock.addFSMVariable)(src, fsm)
            },
            removeFSMState: function (fsm, v) {
                this.$Modal.confirm({
                    title: '删除',
                    content: `<p>删除状态 ${v.name}</p>`,
                    onOk() {
                        for (let i = 0; i < fsm.states.length; i++) {
                            if (fsm.states[i] === v) {
                                if (fsm.startState === i) {
                                    fsm.startState = 0;
                                }
                                fsm.states.splice(i, 1);
                            }
                        }
                        OpenBlock.exportExePackage();
                    }
                });
            },
            removeStruct(src, struct) {
                this.$Modal.confirm({
                    title: '删除',
                    content: `<p>删除数据结构 ${struct.name}</p>`,
                    onOk() {
                        const index = src.structs.indexOf(struct);
                        if (index > -1) {
                            src.structs.splice(index, 1);
                        }
                        OpenBlock.exportExePackage();
                    }
                });
            },
            addFSMState: function (fsm) {
                let state = OpenBlock.addState(fsm);
            },
            addStruct: function (src) {
                if (src && src.structs) {
                    let dt = OpenBlock.addStruct(src);
                }
            },
            addFunction: function (src) {
                if (src && src.functions) {
                    let f = OpenBlock.addFunction(src);
                }
            },
            removeFunction(s, f) {
                this.$Modal.confirm({
                    title: '删除',
                    content: `<p>删除函数 ${f.name}</p>`,
                    onOk() {
                        const index = s.functions.indexOf(f);
                        if (index > -1) {
                            s.functions.splice(index, 1);
                        }
                        OpenBlock.exportExePackage();
                    }
                });
            },
            openBlocklyTab(key, lableBuilder, builderFunc, beforeContentDestroy1) {
                let workspace = this.tabs.find(info => info.key === key);
                if (workspace) {
                    this.showingTabName = workspace.name;
                } else {
                    // let self = this;
                    let newTab = {
                        name: Math.random().toString(16),
                        label: lableBuilder,
                        content: "blockly-editor",
                        key: key,
                        target: null,
                        contentBuilder: (dom, editor) => { return builderFunc(dom, editor, newTab); },
                        beforeContentDestroy(arg) {
                            beforeContentDestroy1(arg);
                        }
                    };
                    this.tabs.push(newTab);
                    this.showingTabName = newTab.name;
                }
                this.siderUsing = null;
            },
            closeTab(key) {
                const index = this.tabs.findIndex(t => t.key === key);
                if (index > -1) {
                    this.tabs.splice(index, 1);
                    if (index >= this.tabs.length) {
                        if (this.tabs.length > 0) {
                            this.showingTabName = this.tabs[this.tabs.length - 1].name;
                        } else {
                            this.showingTabName = null;
                        }
                    }
                }
            },
            openDataTab(tableName) {
                let tab = this.tabs.find(info => info.key === tableName);
                if (tab) {
                    this.showingTabName = tab.name;
                } else {
                    let self = this;
                    let newTab = {
                        name: Math.random().toString(16),
                        label: createElement => {
                            return createElement('div', { attrs: { class: "ns bold" } }, [
                                createElement('icon', { attrs: { type: "ios-folder" } }),
                                tableName,
                                createElement('icon', {
                                    attrs: { type: "ios-close" }, on: {
                                        click: function () {
                                            self.closeTab(tableName);
                                        }
                                    }
                                })]);
                        },
                        content: "data-viewer",
                        key: tableName,
                        target: null,
                        contentBuilder: (dom, editor) => { },
                        beforeContentDestroy(arg) {
                        }
                    };
                    this.tabs.push(newTab);
                    this.showingTabName = newTab.name;
                }
                this.siderUsing = null;
            },
            selectState(src, fsm, state) {
                let self = this;
                this.openBlocklyTab(state, createElement => {
                    return createElement('div', { attrs: { class: "ns bold" } }, [
                        createElement('icon', { attrs: { type: "ios-fastforward" } }),
                        state.name + ":" + fsm.name,
                        createElement('icon', {
                            attrs: { type: "ios-close" }, on: {
                                click: function () {
                                    self.closeTab(state);
                                }
                            }
                        })]);
                },
                    (dom, editor, tab) => {
                        let w = OpenBlock.buildStateBlockly(src, fsm, state, dom);
                        tab.target = w;
                        return w;
                    }, ws => {
                        if (ws.saveCode) {
                            ws.saveCode();
                            ws.dispose();
                        } else {
                            debugger
                        }
                    });
            },
            selectStateByName(srcName, fsmName, stateName) {
                let src = OpenBlock.getSrcByName(srcName);
                let fsm = OpenBlock.getFsmByName(src, fsmName);
                let state = OpenBlock.getStateByName(fsm, stateName);
                this.selectState(src, fsm, state);
            },
            selectStruct(src, struct) {
                let self = this;
                this.openBlocklyTab(struct, (createElement) => {
                    return createElement('div', { attrs: { class: "ns bold" } }, [
                        createElement('icon', { attrs: { type: "logo-codepen" } }),
                        struct.name + ":" + src.name,
                        createElement('icon', {
                            attrs: { type: "ios-close" }, on: {
                                click: function () {
                                    self.closeTab(struct);
                                }
                            }
                        })]);
                },
                    (dom, editor, tab) => {
                        let w = OpenBlock.buildStructBlockly(src, struct, dom);
                        tab.target = w;
                        return w;
                    }, ws => {
                        ws.saveCode();
                        ws.dispose();
                    });
            },
            selectFunctionByName(srcName, funcName) {
                let src = OpenBlock.getSrcByName(srcName);
                let func = OpenBlock.getFuncByName(src, funcName);
                this.selectFunction(src, func);
            },
            selectFunction(src, f) {
                let self = this;
                this.openBlocklyTab(f, (createElement) => {
                    return createElement('div', { attrs: { class: "ns bold" } }, [
                        createElement('icon', { attrs: { type: "md-calculator" } }),
                        f.name + ":" + src.name,
                        createElement('icon', {
                            attrs: { type: "ios-close" }, on: {
                                click: function () {
                                    self.closeTab(f);
                                }
                            }
                        })]);
                },
                    (dom, editor, tab) => {
                        let w = OpenBlock.buildFunctionBlockly(src, f, dom);
                        tab.target = w;
                        return w;
                    }, ws => {
                        if (ws.saveCode) {
                            ws.saveCode();
                            ws.dispose();
                        } else {
                            debugger
                        }
                    });
            },
            addDepends: function (src, depends) {
                if (src.depends.indexOf(depends) === -1) {
                    src.depends.push(depends);
                }
                OpenBlock.BlocklyParser.updateDepends();
                OpenBlock.exportExePackage();
            },
            removeDepends: function (src, depends) {
                let i = src.depends.indexOf(depends);
                if (i > -1) {
                    src.depends.splice(i, 1);
                }
                OpenBlock.BlocklyParser.updateDepends();
                OpenBlock.exportExePackage();
            },
            unloadFile: function (file) {
                this.$Modal.confirm({
                    title: '卸载模块',
                    content: `<p>是否卸载模块 ${file.name}</p>`,
                    onOk() {
                        OpenBlock.BlocklyParser.removeFile(file.name);
                        OpenBlock.exportExePackage();
                    }
                });
            },
            highlightErrBlock: function (errinfo) {
                if (!errinfo.err.src) {
                    return;
                }
                let keyName;
                let type;
                if (errinfo.err.state) {
                    this.selectStateByName(errinfo.err.src,
                        errinfo.err.fsm,
                        errinfo.err.state);
                    keyName = errinfo.err.state;
                    type = 'state';
                } else if (errinfo.err.func) {
                    this.selectFunctionByName(errinfo.err.src, errinfo.err.func);
                    type = 'func'
                    keyName = errinfo.err.func;
                }
                setTimeout(() => {
                    let tab = this.tabs.find(t => {
                        try {
                            return t.key.name === keyName
                                && t.key.type === type
                                && t.target.context.src.name === errinfo.err.src
                                && t.target.context.fsm.name === errinfo.err.fsm;
                        } catch (e) {
                            console.log(e);
                            return false;
                        }
                    });
                    if (tab) {
                        let ubctx = tab.target;
                        let ctx = ubctx.context;
                        let workspace = ctx.workspace;
                        // this.currentBlocklyWorkspace.context.workspace.highlightBlock(errinfo.err.blockId);
                        let blk = workspace.getBlockById(errinfo.err.blockId);
                        if (blk) {
                            blk.select();
                            workspace.centerOnBlock(blk.id);
                        }
                    }
                }, 5);
            },
            importData: function () {
                FileInterface.loadExcel(
                    /**
                     * 
                     * @param {{name:String,content:ArrayBuffer}[]} arrayBufferArray 
                     */
                    (arrayBufferPair) => {
                        OpenBlock.DataImporter.importExcel(arrayBufferPair);
                        OpenBlock.DataImporter.reorganizeData();
                    });
            },
            rebuildData() {
                OpenBlock.DataImporter.reorganizeData();
            },
            allLibs(srcName) {
                if (!srcName) {
                    return [];
                }
                if (OpenBlock.BlocklyParser.loadedFiles.dependingTree) {
                    let unavalibles = this.unavalibleLibs(srcName).map(l => l.value);
                    let arr = OpenBlock.BlocklyParser.loadedFiles.dependingTree.allNodes.map(n => n.value);
                    let r = arr.map(a => { return { "label": a, "key": a, "disabled": a === srcName || (unavalibles.indexOf(a) > -1 && this.editingSrc.depends.indexOf(a) < 0) }; });
                    return r;
                } else {
                    return [];
                }
            },
            avalibleLibs: function (srcName) {
                if (!srcName) {
                    return [];
                }
                if (OpenBlock.BlocklyParser.loadedFiles.dependingTree &&
                    OpenBlock.BlocklyParser.loadedFiles.dependingTree.errors.length === 0) {
                    let r = OpenBlock.BlocklyParser.loadedFiles.dependingTree.avalibleOutNode(srcName);
                    return r;
                }
            },
            unavalibleLibs(srcName) {
                if (!srcName) {
                    return [];
                }
                if (OpenBlock.BlocklyParser.loadedFiles.dependingTree &&
                    OpenBlock.BlocklyParser.loadedFiles.dependingTree.errors.length === 0) {
                    let r = OpenBlock.BlocklyParser.loadedFiles.dependingTree.unavalibleOutNode(srcName);
                    return r;
                }
            },
            changeSrcDepends(newTargetKeys, direction, moveKeys) {
                this.editingSrc.depends = newTargetKeys;
            }
        },
        computed: {
            compiledData() {
                if (OpenBlock.DataImporter && OpenBlock.DataImporter.importedData && OpenBlock.DataImporter.importedData.compiled) {
                    let r = Object.keys(OpenBlock.DataImporter.importedData.compiled);
                    return r;
                } else {
                    return [];
                }
            },
            loadedFiles: function () {
                return [].concat(OpenBlock.BlocklyParser.loadedFiles.srcs, OpenBlock.BlocklyParser.loadedFiles.libs);
            },
            hasError() {
                if (this.compiling) {
                    return false;
                } else {
                    let r = this.errors.length > 0;
                    return r;
                }
            },
            errors: function () {
                let errors = [];
                OpenBlock.BlocklyParser.loadedFiles.srcs.forEach(src => {
                    src._errors.forEach(err => {
                        errors.push({
                            src, err
                        });
                    });
                    if (src.__compiled && src.__compiled.errors) {
                        src.__compiled.errors.forEach(err => { errors.push({ src, err }) });
                    }
                });
                return errors;
            }
        }
    });
    document.getElementById('openblock').style.display = 'block';
};
OpenBlock.onInited(startUI);
OpenBlock.onInited(() => {
    // 后台自动编译打包
    let timeout = -1;
    OpenBlock.wsBuildCbs.push(function (ws) {
        let vm = window.UB_IDE;
        if (vm) {
            ws.addChangeListener(function (e) {
                OpenBlock.Compiler.stop();
                OpenBlock.Linker.stop();
                let time = 800;
                if (e.recordUndo) {
                    time = 200
                }
                if (timeout > 0) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(() => {
                    vm.tabs.forEach(tab => {
                        let t = tab.target;
                        if (t && t.saveCode) {
                            t.saveCode();
                        }
                    });
                    timeout = -1;
                    vm.compiling = true;
                    OpenBlock.exportExePackage(() => {
                        vm.compiling = false;
                        vm.$forceUpdate();
                    });
                }, time);

            });
        }
    });
    OpenBlock.Linker.onFinished((e, r) => {
        if (timeout > 0) {
            clearTimeout(timeout);
        }
        timeout = -1;
    });
    // 处理hash，认为hash是json内容
    if (window.location.hash) {
        let hash = window.location.hash;
        if (hash.length > 0) {
            hash = hash.substring(1);
            hash = decodeURIComponent(hash);
            let hashobj = JSON.parse(hash);
            // 处理 proj参数
            if (hashobj.proj) {
                let projRoot = hashobj.proj.substring(0, hashobj.proj.lastIndexOf('/') + 1);
                requestText(hashobj.proj, data => {
                    if (!data) {
                        return;
                    }
                    let projectInfo = JSON.parse(data);
                    if (projectInfo.src) {
                        let loaded = 0;
                        projectInfo.src.forEach(
                            /**
                             * 
                             * @param {String} s 
                             */
                            (s) => {
                                if (s.indexOf('://') === -1) {
                                    s = projRoot + s;
                                }
                                requestText(s, data => {
                                    if (!data) {
                                        return;
                                    }
                                    let src = JSON.parse(data);
                                    OpenBlock.BlocklyParser.addFiles([src]);
                                    if (++loaded === projectInfo.src.length) {
                                        OpenBlock.exportExePackage();
                                    }
                                });
                            });
                    }
                    if (projectInfo.data) {
                        let loaded = 0;
                        projectInfo.data.forEach(
                            /**
                             * 
                             * @param {String} s 
                             */
                            (s) => {
                                let spath = s;
                                if (spath.indexOf('://') === -1) {
                                    spath = projRoot + s;
                                }
                                requestBin(spath, data => {
                                    OpenBlock.DataImporter.importExcel([{ name: s, content: data }]);
                                    if (++loaded === projectInfo.data.length) {
                                        OpenBlock.DataImporter.reorganizeData();
                                    }
                                });
                            });
                    }
                });
            }
        }
    }
});