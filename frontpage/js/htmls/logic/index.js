/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */

$.ajax({
    url: 'js/htmls/logic/htmls.html',
    dataType: 'text',
    async: false,
    success(data, status, xhr) {
        let componentName = 'ob-logic-sider';
        let template = data;
        Vue.component(componentName, {
            data: function () {
                return {
                    StateTransitionData: null,
                    chart: null
                }
            },
            methods: {
                clear() {
                    this.StateTransitionData = null;
                    if (this.chart) {
                        this.chart.dispose();
                        this.chart = null;
                    }
                },
                /**
                 * 
                 * @param {*} fsm 
                 * @param {StateAnalysesResultFSM} analysed 
                 * @returns {{stateNodes:Object[],links:Object[]}}
                 */
                makeSeries(src, fsm, analysed) {
                    /**
                     * 
                     * nodes:
                    {
                        label() {
                            return "Point A";
                        },
                        tooltip() {
                            return "tooltip A"
                        },
                        onclick() {
                            alert("A");
                        },
                        "id": "0",
                        "name": "A",
                        "category": 0
                    }
                    * links:
                                    {
                                        "source": "1",
                                        "target": "0",
                                        label() {
                                            return "1-0";
                                        },
                                        tooltip() {
                                            return "TOOLTIP 1-0";
                                        },
                                        onclick() {
                                            alert("1-0");
                                        }
                                    }
                     */
                    let stateNodes = [];
                    let links = []
                    let root = this.$root;
                    let self = this;
                    Object.values(fsm.states).forEach(s => {
                        s.category = s === fsm.states[0] ? 0 : 1;
                        s.id = s.name;
                        let onclick = () => {
                            root.selectState(src, fsm, s);
                        };
                        s.onclick = onclick;
                        stateNodes.push(s);
                        if (analysed.states[s.name]) {
                            let outlinks = analysed.states[s.name].relevantStates;
                            outlinks.forEach(target => {
                                let l = {
                                    target,
                                    source: s.name,
                                    onclick
                                }
                                links.push(l);
                            });
                        }
                    });
                    return { stateNodes, links };
                },
                showStateTransition(file, fsm) {
                    if (!OpenBlock.Compiler.compiled[file.name]) {
                        return;
                    }
                    let analysed_m = OpenBlock.Compiler.compiled[file.name].analysed.StateTransition;
                    let analysed = analysed_m.fsm[fsm.name];
                    let series = this.makeSeries(file, fsm, analysed);
                    this.StateTransitionData = { file, fsm, analysed };

                    var chartDom = this.$refs.chart;
                    // var myChart = echarts.init(chartDom);
                    var myChart = echarts.init(chartDom, null, { renderer: 'svg' });
                    var option = {
                        title: {
                            text: file.name + "." + fsm.name,
                            top: 'bottom',
                            left: 'right'
                        },
                        // tooltip: {},
                        tooltip: {
                            show: true,
                            formatter(param) {
                                if (param.data) {
                                    let t = typeof (param.data.tooltip);
                                    if (t === 'function') {
                                        return param.data.tooltip();
                                    }
                                    return param.data.tooltip;
                                }
                            }
                        },
                        series: [
                            {
                                name: fsm.name,
                                type: 'graph',
                                // initLayout: 'circular',
                                layout: 'force',
                                force: {
                                    // initLayout: 'circular',
                                    repulsion: 800
                                },
                                edgeSymbol: ['none', 'arrow'],
                                roam: true,
                                symbolSize: 30,
                                edgeSymbolSize: [4, 10],
                                lineStyle: {
                                    opacity: 0.9,
                                    width: 4,
                                    curveness: 0.5
                                },
                                edgeLabel: {
                                    show: false,
                                    formatter(param) {
                                        if (param.data) {
                                            let t = typeof (param.data.label);
                                            if (t === 'function') {
                                                return param.data.label();
                                            }
                                            return param.data.name;
                                        }
                                    }
                                },
                                label: {
                                    show: true,
                                    // position: "right"
                                    formatter(param) {
                                        if (param.data) {
                                            let t = typeof (param.data.label);
                                            if (t === 'function') {
                                                return param.data.label();
                                            }
                                            return param.data.name;
                                        }
                                    }
                                },
                                // 高亮样式。
                                emphasis: {
                                    focus: 'adjacency',
                                    itemStyle: {
                                        // 高亮时点的颜色。
                                        // color: 'blue'
                                    },
                                    label: {
                                        show: true,
                                        // 高亮时标签的文字。
                                        // formatter: 'This is a emphasis label.'
                                    },
                                    lineStyle: {
                                        // width: 10
                                    },
                                    edgeLabel: {
                                        show: true,
                                        // width: 10,
                                        formatter(param) {
                                            if (param.data) {
                                                let t = typeof (param.data.label);
                                                if (t === 'function') {
                                                    return param.data.label();
                                                }
                                                return param.data.label;
                                            }
                                        }
                                    },
                                },
                                data: series.stateNodes,
                                links: series.links,
                                categories: [
                                    {
                                        name: '初始状态'
                                    },
                                    {
                                        name: '状态'
                                    },
                                    {
                                        name: '消息'
                                    },
                                    {
                                        name: '事件'
                                    },
                                    {
                                        name: '函数'
                                    },
                                ],
                            }
                        ]
                    };
                    // 使用刚指定的配置项和数据显示图表。
                    myChart.setOption(option);

                    myChart.on('click', function (params) {
                        if (params.data.onclick && typeof (params.data.onclick) === 'function') {
                            params.data.onclick();
                        }
                    });
                    this.chart = myChart;
                }
            },
            template: template
        });
        siderComponents.push({ name: componentName, icon: 'md-analytics', tooltip: '逻辑' });
        OpenBlock.onInited(() => {
            UB_IDE.siderUsing = componentName;
        });
    }
});